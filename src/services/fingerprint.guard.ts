import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import { DataService } from './data.service';

@Injectable({ providedIn: 'root'})
export class FingerprintGuard implements CanActivate {
    
    constructor(private dataService: DataService) {}
    
    canActivate(){ 
        if(this.dataService.fingerprint){
            return this.dataService.isFingerPrintAccess;
        }
        return true;           
    }
}