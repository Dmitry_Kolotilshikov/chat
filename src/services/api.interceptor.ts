import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { DataService } from './data.service';


export class ParamInterceptor implements HttpInterceptor {


    constructor(private dataService: DataService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const userId = this.dataService.userId;

        if (userId) {
            const paramReq = req.clone({
                params: req.params.set('userid', userId)
            });
            return next.handle(paramReq)
        } else {
            return next.handle(req);
        }
    }
}