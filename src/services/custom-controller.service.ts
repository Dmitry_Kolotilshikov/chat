import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class CustomControlService {

    private loader: HTMLIonLoadingElement;

    constructor(
        private toastController: ToastController,
        private alertController: AlertController,
        private loadingController: LoadingController
    ) { }

    async showAlert(message: string) {
        const alert = await this.alertController.create({
            header: 'ОЙ',
            subHeader: 'Ошибка',
            message,
            buttons: ['OK']
        });
        await alert.present();
    }

    async showToast(message: string) {
        const toast = await this.toastController.create({
            duration: 1500,
            message
        });
        await toast.present()
    }

    async startLoading() {
        this.loader = await this.loadingController.create({
            duration: 1500,
            message: 'Загрузка...'
        });
        await this.loader.present();
    }

    async stopLoading() {
        if (this.loader) {
            this.loader.dismiss();
        }
    }
}