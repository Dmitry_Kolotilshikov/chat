import { Injectable } from "@angular/core";
import { environment } from 'src/environments/environment.prod';
import { User } from 'src/models/user';
import { HttpClient } from '@angular/common/http';
import { Message } from 'src/models/message';

@Injectable()

export class DataService {
    private readonly url = 'https://' + environment.url;
    public isFingerPrintAccess: boolean;

    constructor(private http: HttpClient) { }

    registration(user: any): Promise<string> {
        return this.http.post<string>(this.url.concat('/users'), user).toPromise();
    }

    set userId(id: string) {
        localStorage.setItem('userid', id);
    }

    get userId(): string {
        const userId = localStorage.getItem('userid');
        return userId ? userId : null;
    }

    set fingerprint(isEnabled: boolean) {
        localStorage.setItem('f_enabled', JSON.stringify(isEnabled));
    }

    get fingerprint(): boolean {
        const isEnabled = localStorage.getItem('f_enabled');
        if (isEnabled) {
            return JSON.parse(isEnabled);
        }
        return false;
    }

    set isDark(isdark: boolean) {
        localStorage.setItem('isdark', JSON.stringify(isdark));
    }

    get isDark(): boolean {
        const isdark = localStorage.getItem('isdark');
        if (isdark) {
            return JSON.parse(isdark);
        }
        return false;
    }


    get isAuto(): boolean {
        const isauto = localStorage.getItem('isauto');
        if (isauto) {
            return JSON.parse(isauto);
        }
        return false;
    }

    set isAuto(value: boolean) {
        localStorage.setItem('isauto', JSON.stringify(value));
    }

    getUserList(): Promise<User[]> {
        return this.http.get<User[]>(this.url.concat('/users')).toPromise();
    }

    auth(user: User): Promise<string> {
        return this.http.post<string>(this.url.concat('/auth'), user).toPromise()
    }

    getUserInfo(): Promise<User> {
        return this.http.get<User>(this.url.concat('/user')).toPromise()
    }

    saveUserChanges(user: User): Promise<any> {
        return this.http.post<any>(this.url.concat('/user'), user).toPromise();
    }


    getMessage(from: number, to: number): Promise<Message[]> {
        return this.http.get<Message[]>(
            this.url.concat('/messages?from=', from.toString(), '&to=', to.toString()))
            .toPromise()
    }

}