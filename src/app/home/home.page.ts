import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './registration/registration.component';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private modalController: ModalController,
    private dataService: DataService) { }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AuthComponent
    });

    return await modal.present();
  }
  async presentRegistrationModal() {
    const modal = await this.modalController.create({
      component: RegistrationComponent
    });

    return await modal.present();
  }

  public get isAuth(): boolean {
    return !!this.dataService.userId
  }
}
