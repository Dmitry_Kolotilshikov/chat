import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { emailValidator } from '../email.validator';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/services/data.service';
import { User } from 'src/models/user';
import { Router } from '@angular/router';

@Component({
    selector: 'app-auth',
    templateUrl: 'auth.component.html',
    styleUrls: ['auth.component.scss']
})

export class AuthComponent implements OnInit {

    public name: FormControl;
    public password: FormControl;

    public userForm: FormGroup;


    constructor(
        private modalController: ModalController,
        private dataService: DataService,
        private router: Router
    ) { }

    ngOnInit() {
        this.createFormFields();
        this.createUserForm()
    }

    private createFormFields(): void {
        this.name = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(32)
        ]);

        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(32)
        ]);
    }

    private createUserForm(): void {
        this.userForm = new FormGroup({
            name: this.name,
            password: this.password
        })
    }

    public async closeModal() {
        await this.modalController.dismiss();
    }

    public auth(): void {
        if (this.userForm.valid) {
            const user: User = this.userForm.value as User;
            this.dataService.auth(user).then(userId => {
                this.dataService.userId = userId
                this.router.navigate(['chat'])
                this.closeModal()
            }).catch(_ => console.log('У тебя ошибка БРО!'));

        }
    }
}
