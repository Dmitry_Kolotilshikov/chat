import { Component, OnInit } from "@angular/core";
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ModalController, LoadingController } from '@ionic/angular';
import { DataService } from 'src/services/data.service';
import { User } from 'src/models/user';
import { Router } from '@angular/router';

@Component({
    selector: 'app-registration',
    templateUrl: 'registration.component.html',
    styleUrls: ['registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    public name: FormControl;
    public password: FormControl;

    public userForm: FormGroup;


    constructor(
        private modalController: ModalController,
        private dataService: DataService,
        private router: Router,
        private loadingControler: LoadingController
    ) { }

    ngOnInit() {
        this.createFormFields();
        this.createUserForm()
    }

    private createFormFields(): void {
        this.name = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(32)
        ]);

        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(32)
        ]);
    }

    private createUserForm(): void {
        this.userForm = new FormGroup({
            name: this.name,
            password: this.password
        })
    }

    public async closeModal() {
        await this.modalController.dismiss();
    }

    public async register() {
        if (this.userForm.valid) {
            const user = {
                name: this.name.value,
                password: this.password.value,
                avatar: '1'
            };
            const loader = await this.loadingControler.create({
                message: 'Подождите...',
                duration: 2000
            });
            loader.present();
            this.dataService.registration(user)
                .then(res => {
                    this.dataService.userId = res;
                    this.closeModal()
                    this.router.navigate(['account']);
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => { loader.dismiss() })
        }
    }
}
