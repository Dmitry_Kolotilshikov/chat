import { Directive, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { ThemeService } from './themes.service';
import { Subscription } from 'rxjs';
import { themes } from './themes';

@Directive({
    selector: '[themeDirective]'
})

export class ThemeDirective implements OnInit, OnDestroy {

    private themeServiceSub: Subscription

    constructor(
        private themeService: ThemeService,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.themeServiceSub = this.themeService.activeTheme.subscribe(themeName => {
            this.updateTheme(themeName)
        })
    }

    private updateTheme(themeName: string): void {
        const theme = themes[themeName]
        const elem: HTMLElement = this.elementRef.nativeElement;
        for (const key in theme) {
            elem.style.setProperty(key, theme[key])
        }
    }

    ngOnDestroy() {
        if (this.themeServiceSub) {
            this.themeServiceSub.unsubscribe()
        }
    }

}