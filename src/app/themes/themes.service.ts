import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { DataService } from 'src/services/data.service';

@Injectable({ providedIn: 'root' })
export class ThemeService {

    public activeTheme = new BehaviorSubject('light');

    constructor(private dataService: DataService) { }

    changeTheme(themeValue: string): void {
        this.activeTheme.next(themeValue)
    }

    enableAutoChange(): void {
        window['plugin'].lightsensor.watchReadings(
            (reading) => {
                let data = JSON.stringify(reading);
                data = JSON.parse(data);
                const themeToggle = data['intensity'] <= 30 ? 'dark' : 'light';
                this.changeTheme(themeToggle);
            }
        )
    }

    disableAutoChange(): void {
        window['plugin'].lightsensor.stop();
        const themeName = this.dataService.isDark ? 'dark' : 'light';
        this.changeTheme(themeName);
    }
}
