export const themes = {
    light: {
        '--background-color': '#f2f2f2',
        '--text-color': '#151515',
        '--navbar-color': '#ff8000'
    },
    dark: {
        '--background-color': '#2e2e2e',
        '--text-color': '#fafafa',
        '--navbar-color': '#1c1c1c'
    }
};