import { NgModule } from "@angular/core";
import { ThemeDirective } from './theme.directive';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ThemeDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ThemeDirective
    ]
})

export class ThemeModule { }