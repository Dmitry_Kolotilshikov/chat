import { NgModule } from "@angular/core";
import { ChatPage } from './chat.page';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TimePipe } from 'src/pipes/time.pipe';
import { MessageComponent } from './message/message.component';
import { WebsocketService } from 'src/services/websocket.service';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        ChatPage,
        TimePipe,
        MessageComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: ChatPage
            }
        ])
    ],
    providers: [
        WebsocketService
    ]
})

export class ChatPageModule {

}