import { Input, Component, } from '@angular/core';
import { Message } from './../../../models/message';
import { User } from 'src/models/user';

@Component({
    selector: 'app-message',
    templateUrl: 'message.component.html',
    styleUrls: ['message.component.scss']
})
export class MessageComponent {


    @Input() message: Message;
    @Input() users: User[];


    public getIsRight(): boolean {

        //return this.currentUserId !== userId
        return false
    }

    // public get name(): string {
    //     return this.users.find(u => u.id === this.message.user_name).name;
    // }
    public get avatar(): string {
        const user = this.users.find(u => u.name === this.message.user_name);
        return `assets/avatars/${user.avatar}.png`
    }


}