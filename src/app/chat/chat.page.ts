import { Component, ViewChild, OnInit } from '@angular/core';
import { Message } from 'src/models/message';
import { User } from 'src/models/user';
import { WebsocketService } from 'src/services/websocket.service';
import { DataService } from 'src/services/data.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { CustomControlService } from 'src/services/custom-controller.service';

@Component({
    selector: 'app-chat',
    templateUrl: 'chat.page.html',
    styleUrls: ['chat.page.scss']
})



export class ChatPage {


    @ViewChild('list', { static: false }) list: any;
    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;

    public messages: Message[] = []

    public users: User[] = [];

    private currentUserName = '';
    private ws: WebSocket;

    public userMessage = '';

    private readonly step = 10;
    private skip = 0;

    constructor(
        private websocketService: WebsocketService,
        private dataService: DataService,
        private customController: CustomControlService
    ) { }

    private initializedPage(): void {
        this.customController.startLoading()
        this.dataService.getUserInfo().then(res => {
            this.currentUserName = res.name
        })
        this.loadUserList();
        this.ws = this.websocketService.connect();
        this.ws.onmessage = (request) => {
            const message: Message = JSON.parse(request.data);
            this.messages.push(message);
            this.scrollToBottom();
        }
    }

    ionViewDidEnter() {
        this.initializedPage()
    }

    private loadMessages(event?): void {
        // const from = this.skip >= this.step ? this.skip - this.step : 0;
        // const to = this.skip === 0 ? this.step : this.skip;
        this.dataService.getMessage(this.skip, this.skip + this.step)
            .then(data => {
                this.messages.unshift(...data);
                this.scrollToBottom();
                if (event) {
                    event.target.complete();
                }
            })
            .catch(_ => this.customController.showAlert('Упс, ошибка загрузки сообщений'))
            .finally(() => this.customController.startLoading())
        this.skip += this.step;
    }

    public scrollToBottom(): void {
        setTimeout(() => {
            const listElement = this.list.el;
            if (listElement) {
                listElement.scrollTo(0, 100000);
            }
        }, 0);
    }

    public send(): void {
        if (this.userMessage.length > 0) {
            const message: Message = {
                user_name: this.currentUserName,
                text: this.userMessage,
                time: Math.round(new Date().getTime() / 1000)
            }
            this.ws.send(JSON.stringify(message));
            this.userMessage = '';
            this.scrollToBottom();
        }
    }


    private loadUserList(): void {
        this.dataService.getUserList()
            .then(res => {
                this.users = res
                this.loadMessages()
            })
            .catch(_ => this.customController.showAlert('Ошибка загрузки'))
            .finally(() => this.customController.startLoading())
    }

    public getIsRight(userName: string): boolean {
        return this.currentUserName === userName
    }

    public loadData(event) {
        this.loadMessages(event)
    }

}   