import { Component } from '@angular/core';

import { Platform, MenuController, IonicModule } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DataService } from 'src/services/data.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { OnInit } from '@angular/core';
import { ThemeService } from './themes/themes.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {



  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuController: MenuController,
    private dataService: DataService,
    private faio: FingerprintAIO,
    private themeService: ThemeService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    if (this.dataService.fingerprint) {
      this.faio.show({
        clientId: 'Fingerprint-Demo'
      })
        .then(_ => this.dataService.isFingerPrintAccess = true)
        .catch(_ => this.dataService.isFingerPrintAccess = false);
    }

    const isAuto = this.dataService.isAuto;
    if (isAuto) {
      this.themeService.enableAutoChange()
    } else {
      const themeName = this.dataService.isDark ? 'dark' : 'light';
      this.themeService.changeTheme(themeName);
    }
  }

  public closeMenu() {
    this.menuController.close()
  }

  public closeApp(): void {
    navigator['app'].exitApp()
  }

}
