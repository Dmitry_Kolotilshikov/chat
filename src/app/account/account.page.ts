import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/models/user';
import { AvatarComponent } from './avatar/avatar.component';
import { Location } from '@angular/common';
import { DataService } from 'src/services/data.service';
import { CustomControlService } from 'src/services/custom-controller.service';
import { ThemeService } from '../themes/themes.service';

@Component({
    selector: 'app-account',
    templateUrl: 'account.page.html',
    styleUrls: ['account.page.scss']
})
export class AccountPage implements OnInit {

    public name: FormControl;
    public password: FormControl;

    public userForm: FormGroup;

    private user: User;

    public avatars: string[] = ['1', '2', '3', '4', '5', '6'];

    public isFingerPrintEnabled: boolean;
    public isDarkThemeEnabled: boolean;

    public loadedImage;

    public isAutoChangeEnabled: boolean;

    constructor(
        private locale: Location,
        private dataService: DataService,
        private customController: CustomControlService,
        private themeService: ThemeService
    ) { }

    ngOnInit() {
        this.customController.startLoading();
        this.isFingerPrintEnabled = this.dataService.fingerprint;
        this.isDarkThemeEnabled = this.dataService.isDark;
        this.loadUserInfo()
            .then(_ => {
                AvatarComponent.selectedAvatar = this.user.avatar;
                this.createFormFields();
                this.createUserForm();
            })
            .catch(_ => this.customController.showAlert('Ошибка Сервера'))
            .finally(() => this.customController.stopLoading());
    }

    public get isHaveUserData(): boolean {
        return !!this.user
    }


    public onThemeChange(event): void {
        const themeName = event.target.checked ? 'dark' : 'light';
        this.themeService.changeTheme(themeName);
        this.dataService.isDark = event.target.checked;
    }

    public onAutoChange(event): void {
        const isAuto = event.target.checked;
        console.log(event.target.checked)
        if (isAuto) {
            this.themeService.enableAutoChange();
        } else {
            this.themeService.disableAutoChange();
        }
        this.isAutoChangeEnabled = this.dataService.isAuto;
    }

    onImageLoad(event): void {
        const f = event.target.files[0];
        const reader = new FileReader();
        // reader.readAsDataURL(event.target.files[0]);
        reader.onload = (e) => {

            const binaryData = e.target['result'];
            const base64string = btoa(binaryData.toString());
            this.loadedImage = base64string;
            console.log(this.loadedImage);

        };

        reader.readAsBinaryString(f);
    }

    private loadUserInfo(): Promise<any> {

        return new Promise((resolve, reject) => {
            this.dataService.getUserInfo().then(res => {
                this.user = res;
                resolve();
            }).catch(_ => reject());
        })
    }

    private createFormFields(): void {
        this.name = new FormControl({ value: this.user.name, disabled: true });
        this.password = new FormControl(this.user.password,
            [Validators.required, Validators.minLength(3), Validators.maxLength(10)]);
    }

    private createUserForm(): void {
        this.userForm = new FormGroup({
            name: this.name,
            password: this.password
        })
    }

    public onAvatarChange(avatar: string): void {
        this.user.avatar = avatar;
    }


    public cancel(): void {
        this.locale.back();
    }

    public saveChanges(): void {
        if (this.userForm.valid) {
            this.customController.startLoading();
            this.dataService.fingerprint = this.isFingerPrintEnabled;
            this.dataService.isAuto = this.isAutoChangeEnabled;
            const user = this.userForm.value as User;
            user.avatar = this.user.avatar;
            user.name = this.name.value;
            this.dataService.saveUserChanges(user)
                .then(_ => {
                    this.customController.showToast('Изменения сохранены');
                })
                .catch(_ => this.customController.showAlert('Изменения не сохранены, ошибка сервера'))
                .finally(() => this.customController.startLoading())
        } else {
            this.customController.showAlert('Введите корректные данные')
        }
    }
}
