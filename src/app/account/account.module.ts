import { NgModule } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AccountPage } from './account.page';
import { AvatarComponent } from './avatar/avatar.component';
import { ThemeModule } from '../themes/theme.module';

@NgModule({
    declarations: [
        AccountPage,
        AvatarComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ThemeModule,
        RouterModule.forChild([
            {
                path: '',
                component: AccountPage
            }
        ])
    ]
})

export class AccountPageModule { }
