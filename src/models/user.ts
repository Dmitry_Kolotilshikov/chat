export interface User {
    name: string,
    id?: number,
    avatar: string,
    password?: string
}